// Deprecated file location:
// This file has moved as of VecGeom v1.2.3
#include <VecGeom/management/ReflFactory.h>

namespace vgdml {
//! Backward compatibility type alias
using ReflFactory = vecgeom::ReflFactory;
}
